const schema = {
    properties: {
        msg: {type: 'string',default:'This is test shout message!!!'},
        show_data: { type: 'boolean', default: false}
    },
    required:['msg']
};

module.exports = schema;