const base = require('base.apicloak.plugin');
const settings_schema = require('./settings.schema.js');

module.exports = function () {
    return Object.assign({}, base('shout', settings_schema), {
        executeAction: function (data, callback) {
            console.log("SHOUTING MESSAGE: ", this.settings.msg);
            
            if(this.settings.show_data){
                console.log("SHOUTING DATA: ",data);
            }
            callback(null, data);
        }
    });
};