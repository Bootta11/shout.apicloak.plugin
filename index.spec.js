const chai = require('chai');
const expect = chai.expect;
const should = chai.should();

describe('Shout plugin', function () {
    let plugin = {};
    before(function (done) {
        plugin = require('./index.js')();
        done();
    });

    describe('action() method', function () {
        let result={};
        before(function (done) {
            plugin.load();
            plugin.action({},function(err,data){
                result.data = data;
                result.error = err;
                done();
            });
        });
        it('should return no error', function () {
            expect(result.error).to.be.null;
        });
        it('should return data object', function () {
            expect(result.data).is.an('object');
        });
    });


});